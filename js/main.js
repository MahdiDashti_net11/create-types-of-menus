function pagination() {

    let offset = $(document).scrollTop();
    let windowHeight = $(window).height();
    let $body = $('body');
  
    switch (true) {
      case (offset > (windowHeight * 3.75)):
        $body.removeClass().addClass('page-5');
        break;
      case (offset > (windowHeight * 2.75)):
        $body.removeClass().addClass('page-4');
        break;
      case (offset > (windowHeight * 1.75)):
        $body.removeClass().addClass('page-3');
        break;
      case (offset > (windowHeight * .75)):
        $body.removeClass().addClass('page-2');
        break;
      default:
        $body.removeClass().addClass('page-1');
     }
  }
  
  pagination();
  
  $(document).on('scroll', pagination);
  
  $(document).on('click', 'a[href^="#"]', function(e) {
      e.preventDefault();
      $('html, body').animate({
          scrollTop: $($.attr(this, 'href')).offset().top
      }, 500);
  });












/////////////////////////////////////////////////////////////////////////////










  jQuery(document).ready(function($){
	//open navigation clicking the menu icon
	$('.cd-nav-trigger').on('click', function(event){
		event.preventDefault();
		toggleNav(true);
	});
	//close the navigation
	$('.cd-close-nav, .cd-overlay').on('click', function(event){
		event.preventDefault();
		toggleNav(false);
	});
	

	function toggleNav(bool) {
		$('.cd-nav-container, .cd-overlay').toggleClass('is-visible', bool);
		$('main').toggleClass('scale-down', bool);
	}

});








/////////////////////////////////////////////////////////////////////////////























jQuery(document).ready(function(){
	if( $('.cd-stretchy-nav2').length > 0 ) {
		var stretchyNavs = $('.cd-stretchy-nav2');
		
		stretchyNavs.each(function(){
			var stretchyNav = $(this),
				stretchyNavTrigger = stretchyNav.find('.cd-nav-trigger2');
			
			stretchyNavTrigger.on('click', function(event){
				event.preventDefault();
				stretchyNav.toggleClass('nav-is-visible');
			});
		});

		$(document).on('click', function(event){
			( !$(event.target).is('.cd-nav-trigger2') && !$(event.target).is('.cd-nav-trigger2 span') ) && stretchyNavs.removeClass('nav-is-visible');
		});
	}
});



// ///////////////////////////////////////////////////////////////////////////////



const triggers = document.querySelectorAll('.cool > li');
        const background = document.querySelector('.dropdownBackground');
        const nav = document.querySelector('.top');

        function handleEnter() {
            this.classList.add('trigger-enter');
            setTimeout(() => this.classList.contains('trigger-enter') && this.classList.add('trigger-enter-active'), 150);
            background.classList.add('open');

            const dropdown = this.querySelector('.dropdown');
            const dropdownCoords = dropdown.getBoundingClientRect();
            const navCoords = nav.getBoundingClientRect();

            const coords = {
                height: dropdownCoords.height,
                width: dropdownCoords.width,
                top: dropdownCoords.top - navCoords.top,
                left: dropdownCoords.left - navCoords.left
            };

            background.style.setProperty('width', `${coords.width}px`);
            background.style.setProperty('height', `${coords.height}px`);
            background.style.setProperty('transform', `translate(${coords.left}px, ${coords.top}px)`);
        }

        function handleLeave() {
            this.classList.remove('trigger-enter', 'trigger-enter-active');
            background.classList.remove('open');
        }

        triggers.forEach(trigger => trigger.addEventListener('mouseenter', handleEnter));
        triggers.forEach(trigger => trigger.addEventListener('mouseleave', handleLeave));